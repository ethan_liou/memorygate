//
//  LJFirmImageView.h
//  MemoryGate
//
//  Created by Liou Yu-Cheng on 2014/2/27.
//  Copyright (c) 2014年 ethan.tanya. All rights reserved.
//

#import <UIKit/UIKit.h>

#define HEIGHT_PADDING 30
#define WIDTH_PADDING 5
#define FIRM_WIDTH 10
#define FIRM_HEIGHT 16
#define FIRM_COUNT 15

@interface LJFirmView : UIView


@end
