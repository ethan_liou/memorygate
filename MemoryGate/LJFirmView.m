//
//  LJFirmImageView.m
//  MemoryGate
//
//  Created by Liou Yu-Cheng on 2014/2/27.
//  Copyright (c) 2014年 ethan.tanya. All rights reserved.
//

#import "LJFirmView.h"

@implementation LJFirmView


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
		self.backgroundColor = [UIColor blackColor];
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
	CGContextRef contextRef = UIGraphicsGetCurrentContext();
	// grid
	CGContextSetRGBFillColor(contextRef, 1.0, 1.0, 1.0, 1.0);
	CGFloat space = (rect.size.width - FIRM_COUNT * FIRM_WIDTH) /FIRM_COUNT;
	CGFloat y_up = (HEIGHT_PADDING - FIRM_HEIGHT) / 2;
	CGFloat y_down = rect.size.height - y_up - FIRM_HEIGHT;
	CGFloat x = 0.0f;
	CGContextFillRect(contextRef, CGRectMake(x, y_up, FIRM_WIDTH/2, FIRM_HEIGHT));
	CGContextFillRect(contextRef, CGRectMake(x, y_down, FIRM_WIDTH/2, FIRM_HEIGHT));
	x += (space+FIRM_WIDTH/2);
	for(int i = 0 ; i < FIRM_COUNT-1 ; i ++){
		CGContextFillRect(contextRef, CGRectMake(x, y_up, FIRM_WIDTH, FIRM_HEIGHT));
		CGContextFillRect(contextRef, CGRectMake(x, y_down, FIRM_WIDTH, FIRM_HEIGHT));
		x += (space+FIRM_WIDTH);
	}
	CGContextFillRect(contextRef, CGRectMake(x, y_up, FIRM_WIDTH/2, FIRM_HEIGHT));
	CGContextFillRect(contextRef, CGRectMake(x, y_down, FIRM_WIDTH/2, FIRM_HEIGHT));
}

@end
