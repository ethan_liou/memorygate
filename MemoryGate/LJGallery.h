//
//  LJGallery.h
//  MemoryGate
//
//  Created by Liou Yu-Cheng on 2014/2/27.
//  Copyright (c) 2014年 ethan.tanya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LJGallery : UIScrollView<UIScrollViewDelegate>

@property (nonatomic,strong) NSMutableArray * photos;

-(void)reloadPhotos:(NSMutableArray*)ps;

@end
