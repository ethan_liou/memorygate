//
//  LJARViewController.m
//  MemoryGate
//
//  Created by Liou Yu-Cheng on 2014/2/27.
//  Copyright (c) 2014年 ethan.tanya. All rights reserved.
//

#import "LJARViewController.h"
#import "LJGallery.h"

@interface LJARViewController (){
	IBOutlet LJGallery * gallery;
}

@end

@implementation LJARViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
	NSMutableArray * arr = [[NSMutableArray alloc] initWithObjects:
					  [UIImage imageNamed:@"sample.jpg"],
					  [UIImage imageNamed:@"sample.jpg"],
					  [UIImage imageNamed:@"sample.jpg"],
					  [UIImage imageNamed:@"sample.jpg"],
					  [UIImage imageNamed:@"sample.jpg"],
					  [UIImage imageNamed:@"sample.jpg"],
					  [UIImage imageNamed:@"sample.jpg"],
					  [UIImage imageNamed:@"sample.jpg"],
					  [UIImage imageNamed:@"sample.jpg"],
					  nil];
	[gallery reloadPhotos:arr];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
