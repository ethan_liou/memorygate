//
//  LJPageViewController.m
//  MemoryGate
//
//  Created by Liou Yu-Cheng on 2013/11/17.
//  Copyright (c) 2013年 ethan.tanya. All rights reserved.
//

#import "LJPageViewController.h"

@interface LJPageViewController ()

@end

@implementation LJPageViewController

@synthesize text;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
	UILabel * label = [[UILabel alloc] initWithFrame:self.view.bounds];
	label.text = self.text;
	[self.view addSubview:label];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
