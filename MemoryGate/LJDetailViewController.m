//
//  LJDetailViewController.m
//  MemoryGate
//
//  Created by Liou Yu-Cheng on 2013/10/9.
//  Copyright (c) 2013年 ethan.tanya. All rights reserved.
//

#import "LJDetailViewController.h"
#import "LJPageViewController.h"

@interface LJDetailViewController ()

@end

@implementation LJDetailViewController

@synthesize key,travel,controller,views;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (UIViewController *)generateViewController:(NSInteger)idx{
	LJPageViewController * vc =[[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"LJPageViewController"];
	vc.text = [NSString stringWithFormat:@"Text %d",idx];
	return vc;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
	self.title = [self.travel.dict objectForKey:@"name"];
	self.views = [[NSMutableArray alloc] initWithCapacity:5];
	[self.views addObject:[self generateViewController:0]];
	[self.views addObject:[self generateViewController:1]];
	[self.views addObject:[self generateViewController:2]];
	[self.views addObject:[self generateViewController:3]];
	[self.views addObject:[self generateViewController:4]];
	self.controller = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStylePageCurl navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
	self.controller.dataSource = self;
	self.controller.delegate = self;
	
	NSArray * initArr = [NSArray arrayWithObject:[self.views objectAtIndex:0]];
	
	[self.controller setViewControllers:initArr direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    [self addChildViewController:self.controller];
    [[self view] addSubview:[self.controller view]];
    [self.controller didMoveToParentViewController:self];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController{
    NSUInteger idx = [self.views indexOfObject:viewController];
    if (idx == 0) {
        return nil;
    }
    
    idx--;
    
    return [self.views objectAtIndex:idx];

}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController{
	NSUInteger idx = [self.views indexOfObject:viewController];
    if (idx == self.views.count - 1) {
        return nil;
    }
    
    idx++;
    
    return [self.views objectAtIndex:idx];
}


@end
