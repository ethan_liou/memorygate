//
//  LJViewController.m
//  MemoryGate
//
//  Created by Liou Yu-Cheng on 2013/10/8.
//  Copyright (c) 2013年 ethan.tanya. All rights reserved.
//

#import "LJViewController.h"
#import "SVProgressHUD.h"
#import "AFNetworking.h"
#import "LJDetailViewController.h"
#import "LJTravel.h"


@interface LJViewController (){
	NSMutableArray * keyList;
}

@end

#define DOMAIN @"http://memory-gate.appspot.com"

@implementation LJViewController

- (IBAction)checkIn{
//	[self loadData:@"hongkong20120907"];
//	if(mReaderView.hidden){
//		mReaderView.hidden = NO;
//		[mReaderView start];
//		mBtn.title = @"Cancel";
//	}
//	else{
//		[mReaderView stop];
//		mReaderView.hidden = YES;
//		mBtn.title = @"CheckIn";
//	}
//	ZBarReaderController *reader = [ZBarReaderController new];
//	reader.delegate = self;
//	if([ZBarReaderController isSourceTypeAvailable:
//		UIImagePickerControllerSourceTypeCamera])
//		reader.sourceType = UIImagePickerControllerSourceTypeCamera;
//	[reader.scanner setSymbology: ZBAR_I25
//						  config: ZBAR_CFG_ENABLE
//							  to: 0];
//	[self presentViewController:reader
//					   animated:YES completion:nil];
}

- (void) showDataInMap:(NSString*)key dict:(NSDictionary*)dict{
	if([keyList indexOfObject:key] == NSNotFound)
		[keyList addObject:key];
	LJTravel * travel = [[LJTravel alloc] initWithKey:key Dict:dict];
	[mMapView addAnnotation:travel];
	[mMapView setCenterCoordinate:travel.coordinate animated:YES];
	[mMapView selectAnnotation:travel animated:YES];
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	
	mReaderView.readerDelegate = self;
	
	// list
	NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
	NSDictionary * dict = [defaults dictionaryRepresentation];
	keyList = [[NSMutableArray alloc] init];
	for(NSString * key in [dict allKeys]){
		NSObject * val = [dict objectForKey:key];
		if([val isKindOfClass:[NSDictionary class]]){
			if([keyList indexOfObject:key] == NSNotFound)
				[keyList addObject:key];
			LJTravel * travel = [[LJTravel alloc] initWithKey:key Dict:val];
			[mMapView addAnnotation:travel];
		}
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) loadData:(NSString*)data{
	NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
	NSDictionary * dict = [defaults dictionaryForKey:data];
	if(dict == nil){
		[SVProgressHUD showWithStatus:@"Loading..."];
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
			NSURL *apiUrl = [NSURL URLWithString:[NSString stringWithFormat:DOMAIN@"/get_detail?travel_id=%@",data]];
			NSString *apiResponse = [NSString stringWithContentsOfURL:apiUrl];
			dispatch_async(dispatch_get_main_queue(), ^{
				[SVProgressHUD dismiss];
				if(apiResponse == nil){
					[SVProgressHUD showErrorWithStatus:@"Not a valid QRCode"];
				}
				else{
					NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:[apiResponse dataUsingEncoding:NSUTF8StringEncoding]
																		  options: NSJSONReadingMutableContainers
																			error: nil];
					[defaults setObject:dict forKey:data];
					[defaults synchronize];
					[self showDataInMap:data dict:dict];
				}
			});
		});
	}
	else{
		[self showDataInMap:data dict:dict];
	}
}

- (void) readerView: (ZBarReaderView*) readerView
     didReadSymbols: (ZBarSymbolSet*) symbols
          fromImage: (UIImage*) image{
	[mReaderView stop];
	mReaderView.hidden = YES;
	mBtn.title = @"CheckIn";
	NSString * data = nil;
	for(ZBarSymbol * symbol in symbols){
		if([symbol.data hasPrefix:@"journey://"]){
			data = [symbol.data substringFromIndex:10];
			break;
		}
	}
	if(data != nil){
		[self loadData:data];
	}
	else{
		[SVProgressHUD showErrorWithStatus:@"Not a valid QRCode"];
	}
}

- (void)selectDest:(UIButton*)sender{
	UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
	LJDetailViewController * controller = (LJDetailViewController *)[storyboard instantiateViewControllerWithIdentifier:@"detail_view"];
	controller.key = [keyList objectAtIndex:sender.tag];
	controller.travel = [[LJTravel alloc] initWithKey:controller.key Dict:[[NSUserDefaults standardUserDefaults] dictionaryForKey:controller.key]];
	[self.navigationController pushViewController:controller animated:YES];
}

- (MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>) annotation{
	if([annotation isKindOfClass:[LJTravel class]]){
		MKPinAnnotationView *newAnnotation = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"annotation1"];
		newAnnotation.pinColor = MKPinAnnotationColorRed;
		newAnnotation.animatesDrop = NO;
		newAnnotation.canShowCallout=YES;
		
		UIButton *button = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
		button.tag = [keyList indexOfObject:((LJTravel *)annotation).key];
		[button addTarget:self action:@selector(selectDest:) forControlEvents:UIControlEventTouchUpInside];
		newAnnotation.rightCalloutAccessoryView=button;
		return newAnnotation;
	}
	else{
		return nil;
	}
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
	ZBarSymbol * symbol = [[info valueForKey:@"ZBarReaderControllerResults"] objectAtIndex:0];
	NSLog(@"%@",NSStringFromCGRect(symbol.bounds));
	[picker popViewControllerAnimated:YES];
}

@end
