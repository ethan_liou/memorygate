//
//  LJAppDelegate.h
//  MemoryGate
//
//  Created by Liou Yu-Cheng on 2013/10/8.
//  Copyright (c) 2013年 ethan.tanya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LJAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
