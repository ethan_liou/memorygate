//
//  LJGallery.m
//  MemoryGate
//
//  Created by Liou Yu-Cheng on 2014/2/27.
//  Copyright (c) 2014年 ethan.tanya. All rights reserved.
//

#import "LJGallery.h"
#import "LJFirmView.h"

@implementation LJGallery

@synthesize photos;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
		self.photos = [[NSMutableArray alloc] init];
		self.delegate = self;
    }
    return self;
}

- (void)renderImageView{
	[self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];

	CGRect rect = self.bounds;
	NSInteger idx = 0;
	
	self.backgroundColor = [UIColor whiteColor];
	for (UIImage * image in self.photos) {
		CGRect r =CGRectMake(idx * rect.size.width, 0, rect.size.width , rect.size.height );
		LJFirmView * fv = [[LJFirmView alloc] initWithFrame:r];
		r = CGRectMake(idx * rect.size.width + WIDTH_PADDING, HEIGHT_PADDING, rect.size.width - 2 * WIDTH_PADDING, rect.size.height - 2 * HEIGHT_PADDING );
		UIImageView * iv = [[UIImageView alloc] initWithFrame:r];
		iv.image = image;
		[self addSubview:fv];
		[self addSubview:iv];
		idx++;
	}
	self.contentSize = CGSizeMake(self.photos.count * rect.size.width, rect.size.height);
		
//	CALayer *layer = self.layer;
//	CATransform3D rotationAndPerspectiveTransform = CATransform3DIdentity;
//	rotationAndPerspectiveTransform.m34 = 1.0 / -500;
//	rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, 45.0f * M_PI / 180.0f, 0.0f, 1.0f, 0.0f);
//	layer.transform = rotationAndPerspectiveTransform;
}

- (void)reloadPhotos:(NSMutableArray *)ps{
	self.photos = ps;
	[self renderImageView];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{

}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
//- (void)drawRect:(CGRect)rect
//{
//	[super drawRect:rect];
//	CGContextRef contextRef = UIGraphicsGetCurrentContext();
//	
//	CGContextSetRGBFillColor(contextRef, 0, 0, 1.0, 1.0);
//	
//	// Draw a circle (filled)
//	CGContextFillEllipseInRect(contextRef, CGRectMake(0, 0, 10, 10));
//}

@end
