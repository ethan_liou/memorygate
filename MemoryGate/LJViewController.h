//
//  LJViewController.h
//  MemoryGate
//
//  Created by Liou Yu-Cheng on 2013/10/8.
//  Copyright (c) 2013年 ethan.tanya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "ZBarSDK.h"

@interface LJViewController : UIViewController<ZBarReaderViewDelegate,MKMapViewDelegate,UIAlertViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>{
	IBOutlet ZBarReaderView * mReaderView;
	IBOutlet MKMapView * mMapView;
	IBOutlet UIBarButtonItem * mBtn;
}

- (void) loadData:(NSString*)data;

@end
