//
//  LJTravel.m
//  MemoryGate
//
//  Created by Liou Yu-Cheng on 2013/10/9.
//  Copyright (c) 2013年 ethan.tanya. All rights reserved.
//

#import "LJTravel.h"

@implementation LJTravel

@synthesize key,coordinate,title,subtitle,dict;

-(id)initWithKey:(NSString*)k Dict:(NSDictionary*)d{
	self = [super init];
	if(self){
		self.key = k;
		self.dict = d;
		NSString * location = [d objectForKey:@"geopoint"];
		NSArray *firstSplit = [location componentsSeparatedByString:@","];
		coordinate.latitude = [[firstSplit objectAtIndex:0] doubleValue];
		coordinate.longitude = [[firstSplit objectAtIndex:1] doubleValue];
		self.title = [dict objectForKey:@"name"];
		self.subtitle = [NSString stringWithFormat:@"%@ ~ %@",[dict objectForKey:@"from_date"],[dict objectForKey:@"to_date"]];
	}
	return self;
}

@end
