//
//  LJDetailViewController.h
//  MemoryGate
//
//  Created by Liou Yu-Cheng on 2013/10/9.
//  Copyright (c) 2013年 ethan.tanya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LJTravel.h"

@interface LJDetailViewController : UIViewController<UIPageViewControllerDataSource,UIPageViewControllerDelegate>

@property (nonatomic,strong) NSString * key;
@property (nonatomic,strong) LJTravel * travel;
@property (nonatomic,strong) UIPageViewController * controller;
@property (nonatomic,strong) NSMutableArray * views;

@end
