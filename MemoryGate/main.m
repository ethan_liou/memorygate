//
//  main.m
//  MemoryGate
//
//  Created by Liou Yu-Cheng on 2013/10/8.
//  Copyright (c) 2013年 ethan.tanya. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LJAppDelegate.h"

int main(int argc, char *argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([LJAppDelegate class]));
	}
}
