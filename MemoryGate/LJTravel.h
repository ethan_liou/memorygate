//
//  LJTravel.h
//  MemoryGate
//
//  Created by Liou Yu-Cheng on 2013/10/9.
//  Copyright (c) 2013年 ethan.tanya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface LJTravel : NSObject<MKAnnotation>

@property (nonatomic,strong) NSString *key;
@property (nonatomic,readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic,strong) NSString *subtitle;
@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSDictionary * dict;

-(id)initWithKey:(NSString*)k Dict:(NSDictionary*)d;

@end
