//
//  LJPageViewController.h
//  MemoryGate
//
//  Created by Liou Yu-Cheng on 2013/11/17.
//  Copyright (c) 2013年 ethan.tanya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LJPageViewController : UIViewController

@property (nonatomic,strong) NSString * text;

@end
